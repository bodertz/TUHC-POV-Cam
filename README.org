* Fork of UHC-POV-Cam

A fork of [[https://flaringk.github.io/Portfolio/][FlaringK's port]] of [[madman-bob][https://github.com/madman-bob/Homestuck-POV-Cam]]'s Homestuck POV Cam Chrome extension to the [[https://bambosh.dev/unofficial-homestuck-collection/][Unofficial Homestuck Collection]].

Download this mod as a ZIP file, extract it, and place it into your mods folder within the UHC asset pack. [Here's a guide for installing mods](https://github.com/Bambosh/unofficial-homestuck-collection/blob/main/MODDING.md#installing-mods)

** Changelog

*** v0.7
  - Be forked by Bodertz
  - Fixed links being broken on A6A5A1x2 COMBO pages
  - Fixed HIC being missing on A6A5A1x2 COMBO pages
  - Fixed links on Collide and Act 7 being hardcoded and unresponsive to hidden group settings
  - Fixed a number of timeline issues
    - Typo in gamzee.txt where page 9225 was written as 9255
    - Only show one tavrisprite entry when travrisplode
    - Fix DOTA timeline intersection (same effect as the fix in v0.4.1, but now fixed in the source: vriska.txt)
    - Rename baby versions of John and Rose to Baby John and Baby Rose
    - Rename baby version of Bro to Baby Bro
      - TODO: Consider naming him Baby Dirk/Baby Bro, and dealing with the split in the same way as tavrisprite
  - Added option to hide the next page button if a character links to the same page
  - Added readable timeline files and compiler
    - compiler now outputs json instead of js

*** v0.6.1
- Fixed white hover text in scratch theme
- Fixed Horuss's name in the timeline povmap

*** v0.6
- Redid inserting link code
- Finished Timeline map (/povmap)

*** v0.5
- Added "Always display names" option
- Added Timeline Map (Unfishined)

*** v0.4.1
- Updated DOTA timeline intersection

*** v0.4
- Added hover text
- Fixed styling on Collide
- Fixed paths on Act 7

*** v0.3

- Fixed undefined pages bug (Signaled end of a timeline)
- Added end of Timeline signals
- Fixed pages linking beyond 10000 causing errors

*** v0.2

- Accounted for missing page indexes within timelines.json
- Revised john.png
- Fixed tab bug
